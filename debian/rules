#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

# Include dpatch stuff.
include /usr/share/dpatch/dpatch.make

PYVERS=$(shell pyversions -vr)
DEB_UPSTREAM_VERSION=$(shell dpkg-parsechangelog | sed -rne 's,^Version: ([^-]+).*,\1,p')
REVISION = $(shell dpkg-parsechangelog | sed -rne 's,^Version: .*~r([^-]+).*,\1,p')

clean: unpatch
	dh_testdir
	dh_testroot
	rm -rf build
	find . -name *\.py[co] -exec rm {} \;
	dh_clean

build:

install: patch $(PYVERS:%=install-python%)
install-python%:
	python$* setup.py install \
		--single-version-externally-managed \
		--root $(CURDIR)/debian/python-authkit

	# remove the stupid executable flag from README.txt files
	find . -name README.txt | xargs chmod -x
	# remove the executable flag from example scripts
	chmod -x examples/docs/form2.py examples/docs/form_no_user_in_cookie.py

# Build architecture-dependent files here.
binary-arch:

# Build architecture-independent files here.
binary-indep: build install
	dh_testdir
	dh_testroot
	dh_install debian/lintian-overrides/python-authkit /usr/share/lintian/overrides/
	dh_installchangelogs
	dh_installdocs --exclude=pudge_template --exclude=scripts
	dh_python2
	dh_compress -X.py
	dh_fixperms
	dh_installdeb
	dh_gencontrol
	dh_md5sums
	dh_builddeb

# Build architecture-dependent files here.
binary-arch: build install
# We have nothing to do by default.

binary: binary-indep binary-arch

get-orig-source:
	if [ "$(REVISION)" = "" ]; then uscan --force-download --rename --repack; else \
	svn -q export -r $(REVISION) http://authkit.org/svn/AuthKit/trunk/ python-authkit-$(DEB_UPSTREAM_VERSION) &&\
        tar -zcf ./python-authkit_$(DEB_UPSTREAM_VERSION).orig.tar.gz python-authkit-$(DEB_UPSTREAM_VERSION);\
	rm -rf python-authkit-$(DEB_UPSTREAM_VERSION) ; fi

.PHONY: build clean binary-indep binary-arch install
